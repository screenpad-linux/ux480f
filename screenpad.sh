#!/bin/bash

#echo '    Option "ModeValidation" "NoDFPNativeResolutionCheck,NoVirtualSizeCheck,NoMaxPClkCheck,NoHorizSyncCheck,NoEdidDFPMaxSizeCheck,NoVertRefreshCheck,NoWidthAlignmentCheck,NoEdidMaxPClkCheck,NoMaxSizeCheck"' | sudo tee -a /usr/share/X11/xorg.conf.d/10-nvidia.conf
sudo perl -0777 -i.original -pe 's/Driver "nvidia"\n    Option /Driver "nvidia"\n\tOption "ModeValidation" "NoDFPNativeResolutionCheck,NoVirtualSizeCheck,NoMaxPClkCheck,NoHorizSyncCheck,NoEdidDFPMaxSizeCheck,NoVertRefreshCheck,NoWidthAlignmentCheck,NoEdidMaxPClkCheck,NoMaxSizeCheck"\n\tOption /igs' /usr/share/X11/xorg.conf.d/10-nvidia.conf
echo "sudo reboot"
echo "optimus-manager --switch nvidia --no-confirm"
echo '\_SB.ATKD.WMNB 0x0 0x53564544 b32000500FF000000' | sudo tee /proc/acpi/call
xrandr --newmode "screenpad" 140.00 1080 1150 1170 1195 1920 1924 1926 1930 -hsync -vsync
xrandr --addmode HDMI-0 "screenpad"
xrandr --output HDMI-0 --mode "screenpad" --rotate "right"
alias screenpadmax="echo '\_SB.ATKD.WMNB 0x0 0x53564544 b32000500FF000000' | sudo tee /proc/acpi/call"
alias screenpad75="echo '\_SB.ATKD.WMNB 0x0 0x53564544 b32000500BF000000' | sudo tee /proc/acpi/call"
alias screenpad50="echo '\_SB.ATKD.WMNB 0x0 0x53564544 b320005007F000000' | sudo tee /proc/acpi/call"
alias screenpad25="echo '\_SB.ATKD.WMNB 0x0 0x53564544 b3200050040000000' | sudo tee /proc/acpi/call"
alias screenpadmin="echo '\_SB.ATKD.WMNB 0x0 0x53564544 b3200050001000000' | sudo tee /proc/acpi/call"
alias screenpad0="echo '\_SB.ATKD.WMNB 0x0 0x53564544 b3200050000000000' | sudo tee /proc/acpi/call"